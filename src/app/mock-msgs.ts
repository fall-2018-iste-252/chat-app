import { Msg } from './msg';

export const MESSAGES: Msg[] = [
    {
        id: 1,
        value: 'Hi!'
    },
    {
        id: 2,
        value: 'Happy Friday!'
    }
];
