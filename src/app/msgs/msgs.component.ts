import { Component, OnInit } from '@angular/core';
import { Msg } from "../msg";
import { MESSAGES } from "../mock-msgs";
import { MessageService } from "../message.service";   

@Component({
  selector: 'app-msgs',
  templateUrl: './msgs.component.html',
  styleUrls: ['./msgs.component.scss']
})
export class MsgsComponent implements OnInit {
  // msg: string;
  newMessage: Msg = {
    id: 0,
    value: ''
  };

  msg: Msg;
  messages: Msg[];

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    // this.msg = 'Hello!'
    this.msg = {
      id: 1,
      value: 'I am in class.'
    };
    this.getMessages();
  }

  getMessages(): void {
    this.messageService.getMessages().subscribe(data => {
      console.log(data);
      this.messages = <Msg[]> data.data;
      // Reorder our list
      this.messages = this.messages.sort((a, b) => a.id - b.id);
    });
  }
  saveMessage() {
    // this.messages.push(this.newMessage);
    this.messageService.addMessage(this.newMessage).subscribe(data => {
      console.log(data);
      if (data.data) {
        const created = <Msg> data.data;
        this.messages.push(created);
      }
      // this.getMessages();
    });
    this.newMessage = {
      id: 0,
      value: ''
    };
  }
}
