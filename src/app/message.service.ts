import { Injectable } from '@angular/core';
import { Msg } from './msg';
import { MESSAGES } from './mock-msgs';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Accept': 'application/json',
    'Content-Type':  'application/x-www-form-urlencoded'
  })
};

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  messages: Msg[];
  httpUrl = '/~mskics/iste252/chat-api/index.php';

  constructor(private client: HttpClient) {
    this.messages = MESSAGES;
  }


  // getMessages(): Observable<Msg[]> {
  //   return of(this.messages);
  // }

  getMessages(): Observable<any> {
    return this.client.get<any>(this.httpUrl);
  }

  // addMessage(message: Msg): void {
  //   this.messages.push(message);
  // }
  addMessage(message: Msg): Observable<any> {
    console.log("Will set message " + message);
    const body = new HttpParams()
      .set(`message`, message.value);
    return this.client.post(this.httpUrl, body.toString(), httpOptions);
    // this.messages.push(message);
  }
}
